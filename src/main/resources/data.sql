INSERT INTO user (id, address, name, email, password, role) VALUES
  (1, 'calle Moncloa, 21. Planta 1. Puerta C. Madrid, 28012', 'Juan Torres', 'juan@gmail.com', 'patata', 'cliente'),
  (2, NULL, 'Admin', 'admin@gmail.com', 'adminadmin', 'administrador'),
  (3, 'calle Plaza, 55. Planta 3. Puerta A. Madrid, 22023', 'Pepe Sánchez', 'pepe@gmail.com', 'pepe', 'cliente'),
  (4, NULL, 'Cocinero', 'cocinero@gmail.com', 'cocinerococinero', 'cocinero'),
  (5, NULL, 'Repartidor', 'repartidor@gmail.com', 'repartidor', 'repartidor');

INSERT INTO product (id, category, description, is_in_daily_menu, is_in_menu, name, price) VALUES
  (1, "ensalada", "muy saludable", TRUE, TRUE, "Ensalada César", 12),
  (2, "principal", "Simplemente carne", FALSE, TRUE, "Carne", 10),
  (3, "entrante", "Pastelitos crujientes y ondulados con verduras mixtas", TRUE, TRUE, "Samosa", 7),
  (4, "entrante", "Albóndigas de verduras al vapor y mezcladas con especias nepalesas con sopa de verduras", TRUE, TRUE, "MoMo Vegetal", 8),
  (5, "entrante", "Camarones con crema de zanahoria", FALSE, TRUE, "Jhinge Machhako Raas", 10),
  (6, "principal", "Langostinos marinados al tandoor, tomate, chiles verdes, cebolla, jengibre y pimiento", TRUE, TRUE, "Malekhuko Jhingemachha", 13),
  (7, "principal", "Trozos de filete de pollo guisado con cebolla, pimiento, tomate fresco, jengibre, ají verde, salsa de ajo", TRUE, TRUE, "Karahi Kukhuro", 11),
  (8, "principal", "Brócoli, coliflor, requesón, champiñones, papa, pimiento, cebolla en salsa picante de jengibre y ajo", TRUE, TRUE, "Karai Tarkari", 12),
  (9, "postre", "Bolas de queso con jarabe de cardamomo", TRUE, TRUE, "Lal Mohan", 6),
  (10, "postre", "Helado refrescante", TRUE, TRUE, "Bola de helado", 5),
  (11, "bebida", "Simplemente agua", TRUE, TRUE, "Agua", 4)
  ;

INSERT INTO customer_order (id, amount, date, delivery_time,shipping_address, state, user_id) VALUES
  (1, 100, '2021-01-12 14:02', '2021-01-12 18:02', 'calle Salvador Allende, 12. Madrid', 'recibido', 1),
  (2, 13, '2021-01-02 10:10', '2021-01-02 11:00', 'calle Salvador Allende, 12. Madrid', 'entregado', 1);

INSERT INTO menu (id) VALUES
  (1),
  (2);

INSERT INTO product_menu (menu_id, product_id) VALUES
  (1, 1), (1, 6), (1, 10), (1,11),
  (2, 3), (2, 8), (2, 10), (2,11)
  ;

INSERT INTO product_customer_order (customer_order_id, product_id, amount) VALUES
  (1, 1, 2), (1, 2, 2),
  (2, 2, 1);

INSERT INTO menu_customer_order (customer_order_id, menu_id, amount) VALUES
  (1, 1, 2), (1, 2, 1);
