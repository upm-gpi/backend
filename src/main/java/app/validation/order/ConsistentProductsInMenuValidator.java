package app.validation.order;

import app.entity.Product;
import app.model.order.request.CustomerOrderCreateRequest;
import app.repository.ProductRepository;
import app.validation.order.annotations.ConsistentProductsInMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.Optional;

import static app.entity.Product.Category;

@Component
public class ConsistentProductsInMenuValidator
  implements
    ConstraintValidator<ConsistentProductsInMenu, List<CustomerOrderCreateRequest.Menu>> {

  @Autowired
  private ProductRepository productRepository;

  @Override
  public boolean isValid(
    List<CustomerOrderCreateRequest.Menu> value,
    ConstraintValidatorContext context
  ) {
    return value
      .stream()
      .allMatch(
        menu ->
          validateProduct(menu.getStarterId(), Category.entrante) &&
          validateProduct(menu.getMainId(), Category.principal) &&
          validateProduct(menu.getDessertId(), Category.postre) &&
          validateProduct(menu.getDrinkId(), Category.bebida)
      );
  }

  private boolean validateProduct(Long id, Category expectedCategory) {
    Optional<Product> productOpt = productRepository.findById(id);
    if (productOpt.isEmpty() || !productOpt.get().getIsInDailyMenu()) {
      return false;
    }

    Category productCategory = productOpt.get().getCategory();
    return (
      (
        productCategory.equals(expectedCategory) ||
        ( // unique special case
          productCategory.equals(Category.ensalada) &&
          expectedCategory.equals(Category.entrante)
        )
      )
    );
  }
}
