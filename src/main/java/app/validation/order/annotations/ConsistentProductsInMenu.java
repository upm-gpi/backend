package app.validation.order.annotations;

import app.validation.order.ConsistentProductsInMenuValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ConsistentProductsInMenuValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ConsistentProductsInMenu {
  String message() default "Invalid";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
