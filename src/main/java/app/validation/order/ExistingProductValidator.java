package app.validation.order;

import app.model.order.request.CustomerOrderCreateRequest;
import app.repository.ProductRepository;
import app.validation.order.annotations.ExistingProducts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ExistingProductValidator
  implements
    ConstraintValidator<ExistingProducts, List<CustomerOrderCreateRequest.Product>> {

  @Autowired
  private ProductRepository productRepository;

  @Override
  public boolean isValid(
    List<CustomerOrderCreateRequest.Product> value,
    ConstraintValidatorContext context
  ) {
    List<Long> productsIds = value
      .stream()
      .map(CustomerOrderCreateRequest.Product::getId)
      .collect(Collectors.toList());

    return productRepository.findAllById(productsIds).size() == value.size();
  }
}
