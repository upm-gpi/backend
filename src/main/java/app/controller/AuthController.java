package app.controller;

import app.entity.User;
import app.model.UserDTO;
import app.model.auth.request.LoginRequest;
import app.model.auth.request.RegisterRequest;
import app.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AuthController {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private ModelMapper modelMapper;

  @PostMapping("/login")
  public ResponseEntity<UserDTO> login(@RequestBody LoginRequest loginRequest) {
    var user = userRepository.findByEmail(loginRequest.getEmail());

    if (user.isEmpty()) {
      return ResponseEntity.notFound().build();
    } else if (!user.get().getPassword().equals(loginRequest.getPassword())) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    return ResponseEntity.ok(modelMapper.map(user.get(), UserDTO.class));
  }

  @PostMapping("/register")
  public ResponseEntity<UserDTO> register(
    @Valid @RequestBody RegisterRequest registerRequest
  ) {
    User user = modelMapper.map(registerRequest, User.class);
    user = userRepository.save(user);
    return ResponseEntity.ok(modelMapper.map(user, UserDTO.class));
  }
}
