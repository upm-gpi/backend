package app.controller;

import app.model.UserDTO;
import app.repository.UserRepository;
import app.util.ModelMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private ModelMapperUtils modelMapperUtils;

  @GetMapping("/users")
  public ResponseEntity<List<UserDTO>> test() {
    List<UserDTO> data = modelMapperUtils.map(
      userRepository.findAll(),
      UserDTO.class
    );

    return ResponseEntity.ok(data);
  }
}
