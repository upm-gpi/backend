package app.controller;

import static app.entity.CustomerOrder.State;
import static app.entity.User.Role;

import app.entity.*;
import app.model.CustomerOrderDTO;
import app.model.order.request.CustomerOrderCreateRequest;
import app.model.order.request.CustomerOrderDeleteRequest;
import app.model.order.request.CustomerOrderUpdateStateRequest;
import app.service.CustomerOrderService;
import app.service.MenuService;
import app.service.ProductService;
import app.service.UserService;
import app.util.ModelMapperUtils;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CustomerOrderController {

  @Autowired
  private CustomerOrderService customerOrderService;

  @Autowired
  private ProductService productService;

  @Autowired
  private UserService userService;

  @Autowired
  private MenuService menuService;

  @Autowired
  private ModelMapperUtils modelMapperUtils;

  @GetMapping("/orders")
  public ResponseEntity<List<CustomerOrderDTO>> customerOrders(
    @RequestParam Long customerId
  ) {
    Optional<User> user = userService.getById(customerId);

    if (user.isEmpty()) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    List<CustomerOrderDTO> orders = modelMapperUtils.map(
      customerOrderService.getByUser(user.get()),
      CustomerOrderDTO.class
    );

    return ResponseEntity.ok(orders);
  }

  @PatchMapping("/order/state")
  public ResponseEntity<Void> updateState(
    @RequestBody CustomerOrderUpdateStateRequest customerOrderUpdateStateRequest
  ) {
    Optional<CustomerOrder> orderOpt = customerOrderService.getById(
      customerOrderUpdateStateRequest.getOrderId()
    );
    Optional<User> userOpt = userService.getById(
      customerOrderUpdateStateRequest.getUserId()
    );

    if (orderOpt.isEmpty() || userOpt.isEmpty()) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    Role userRole = userOpt.get().getRole();
    State newState = customerOrderUpdateStateRequest.getNewState();
    if (!customerOrderService.canUpdateState(userRole, newState)) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    CustomerOrder order = orderOpt.get();
    order.setState(newState);
    customerOrderService.save(order);

    return ResponseEntity.ok().build();
  }

  @PostMapping("/order")
  public ResponseEntity<CustomerOrderDTO> createOrder(
    @Valid @RequestBody CustomerOrderCreateRequest customerOrderCreateRequest
  ) {
    boolean isOrderEmpty =
      customerOrderCreateRequest.getProducts().isEmpty() &&
      customerOrderCreateRequest.getMenus().isEmpty();

    boolean isMenuNotAllowed =
      !customerOrderCreateRequest.getMenus().isEmpty() &&
      !menuService.isMenuOnTime(customerOrderCreateRequest.getDeliveryTime());

    if (isOrderEmpty || isMenuNotAllowed) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    CustomerOrder order = new CustomerOrder();
    order.setShippingAddress(customerOrderCreateRequest.getShippingAddress());
    order.setDeliveryTime(customerOrderCreateRequest.getDeliveryTime());
    order.setAmount(customerOrderCreateRequest.getAmount());
    order.setDate(Timestamp.from(Instant.now()));
    order.setState(State.recibido);
    order.setUser(
      userService.getById(customerOrderCreateRequest.getUserId()).orElse(null)
    );

    List<ProductCustomerOrder> productCustomerOrders = customerOrderCreateRequest
      .getProducts()
      .stream()
      .map(
        summarizedProduct -> {
          ProductCustomerOrder productCustomerOrder = new ProductCustomerOrder();

          productCustomerOrder.setAmount(summarizedProduct.getAmount());
          productCustomerOrder.setCustomerOrder(order);
          productCustomerOrder.setProduct(
            productService.getById(summarizedProduct.getId()).get()
          );

          return productCustomerOrder;
        }
      )
      .collect(Collectors.toList());
    order.setProductCustomerOrderList(productCustomerOrders);

    List<MenuCustomerOrder> menuCustomerOrders = customerOrderCreateRequest
      .getMenus()
      .stream()
      .map(
        summarizedMenu -> {
          List<Product> menuProducts = productService.getAllByIds(
            List.of(
              summarizedMenu.getStarterId(),
              summarizedMenu.getMainId(),
              summarizedMenu.getDessertId(),
              summarizedMenu.getDrinkId()
            )
          );

          MenuCustomerOrder menuCustomerOrder = new MenuCustomerOrder();
          menuCustomerOrder.setAmount(summarizedMenu.getAmount());
          menuCustomerOrder.setCustomerOrder(order);
          menuCustomerOrder.setMenu(
            menuService.getMenuInsertIfNotExists(menuProducts)
          );

          return menuCustomerOrder;
        }
      )
      .collect(Collectors.toList());
    order.setMenuCustomerOrderList(menuCustomerOrders);

    var res = customerOrderService.save(order);
    return ResponseEntity.ok(modelMapperUtils.map(res, CustomerOrderDTO.class));
  }

  @DeleteMapping("/order")
  public ResponseEntity<Void> deleteOrder(
    @Valid @RequestBody CustomerOrderDeleteRequest customerOrderDeleteRequest
  ) {
    Optional<User> userOpt = userService.getById(
      customerOrderDeleteRequest.getUserId()
    );
    Optional<CustomerOrder> orderOpt = customerOrderService.getById(
      customerOrderDeleteRequest.getOrderId()
    );

    if (userOpt.isEmpty() || orderOpt.isEmpty()) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    if (
      !customerOrderService.canDeleteOrder(
        userOpt.get().getRole(),
        orderOpt.get()
      )
    ) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    customerOrderService.deleteOrder(orderOpt.get());
    return ResponseEntity.ok().build();
  }
}
