package app.controller;

import app.entity.Product;
import app.model.ProductDTO;
import app.model.product.response.ProductsDailyMenuResponse;
import app.service.ProductService;
import app.util.ModelMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static app.entity.Product.Category;

@RestController
public class ProductController {

  @Autowired
  private ProductService productService;

  @Autowired
  private ModelMapperUtils modelMapperUtils;

  @GetMapping("/products")
  public ResponseEntity<List<ProductDTO>> getProductsByX(
    @RequestParam Optional<Category> category,
    @RequestParam Optional<Boolean> inMenu,
    @RequestParam Optional<Boolean> inDailyMenu
  ) {
    List<Product> products = productService.getByFilter(
      category,
      inMenu,
      inDailyMenu
    );
    List<ProductDTO> responseData = modelMapperUtils.map(
      products,
      ProductDTO.class
    );

    return ResponseEntity.ok(responseData);
  }

  @GetMapping("/products/categories")
  public ResponseEntity<Category[]> getCategories() {
    return ResponseEntity.ok(Product.Category.values());
  }

  @GetMapping("/products/dailyMenu")
  public ResponseEntity<ProductsDailyMenuResponse> dailyMenu() {
    ProductsDailyMenuResponse response = new ProductsDailyMenuResponse();
    response.setStarters(new ArrayList<>());
    response.setMains(new ArrayList<>());
    response.setDesserts(new ArrayList<>());
    response.setDrinks(new ArrayList<>());

    productService
      .getByFilter(Optional.empty(), Optional.empty(), Optional.of(true))
      .stream()
      .map(product -> modelMapperUtils.map(product, ProductDTO.class))
      .forEach(
        product -> {
          switch (product.getCategory()) {
            case entrante:
            case ensalada:
              response.getStarters().add(product);
              break;
            case principal:
              response.getMains().add(product);
              break;
            case postre:
              response.getDesserts().add(product);
              break;
            case bebida:
              response.getDrinks().add(product);
              break;
          }
        }
      );

    return ResponseEntity.ok(response);
  }
}
