package app.service;

import static app.entity.CustomerOrder.State;

import app.entity.CustomerOrder;
import app.entity.User;
import app.repository.CustomerOrderRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerOrderService {

  @Autowired
  private CustomerOrderRepository customerOrderRepository;

  public List<CustomerOrder> getByUser(User user) {
    List<CustomerOrder> res = null;

    switch (user.getRole()) {
      case cliente:
        res = customerOrderRepository.findByUserId(user.getId());
        break;
      case cocinero:
        res =
          customerOrderRepository.findByStateIn(
            List.of(State.recibido, State.preparando)
          );
        break;
      case repartidor:
        res = customerOrderRepository.findByStateIn(List.of(State.entregando));
        break;
      case administrador:
        res = customerOrderRepository.findAll();
        break;
    }

    return res;
  }

  public Optional<CustomerOrder> getById(Long id) {
    if (id == null) {
      return Optional.empty();
    }

    return this.customerOrderRepository.findById(id);
  }

  public boolean canUpdateState(User.Role role, State newState) {
    switch (role) {
      case administrador:
        return true;
      case cocinero:
        return (
          List
            .of(State.recibido, State.preparando, State.entregando)
            .contains(newState)
        );
      case repartidor:
        return List.of(State.entregando, State.entregado).contains(newState);
      default: // cliente
        return false;
    }
  }

  public boolean canDeleteOrder(User.Role role, CustomerOrder customerOrder) {
    switch (role) {
      case administrador:
        return true;
      case cliente:
        LocalDateTime now = LocalDateTime.now(), orderTime = customerOrder
          .getDate()
          .toLocalDateTime();
        System.out.println(now);
        System.out.println(orderTime);
        return (
          now.isAfter(orderTime) && now.isBefore(orderTime.plusMinutes(10L))
        );
      default:
        return false;
    }
  }

  public void deleteOrder(CustomerOrder customerOrder) {
    customerOrderRepository.delete(customerOrder);
  }

  public CustomerOrder save(CustomerOrder customerOrder) {
    return customerOrderRepository.save(customerOrder);
  }
}
