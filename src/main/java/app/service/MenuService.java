package app.service;

import app.entity.Menu;
import app.entity.Product;
import app.entity.ProductMenu;
import app.repository.MenuRepository;
import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenuService {

  @Autowired
  private MenuRepository menuRepository;

  public Menu addMenu(List<Product> products) {
    Menu menu = new Menu();
    List<ProductMenu> productsMenu = StreamEx
      .of(products)
      .map(
        product -> {
          ProductMenu pm = new ProductMenu();
          pm.setProduct(product);
          pm.setMenu(menu);

          return pm;
        }
      )
      .toList();
    menu.setProductMenuList(productsMenu);

    return menuRepository.save(menu);
  }

  public Menu getMenuInsertIfNotExists(List<Product> products) {
    String listOfIds = StreamEx
      .of(products)
      .mapToLong(Product::getId)
      .sorted()
      .mapToObj(Long::toString)
      .collect(Collectors.joining(","));

    Optional<Menu> menu = menuRepository.isAMenu(listOfIds);

    return menu.orElseGet(() -> addMenu(products));
  }

  public boolean isMenuOnTime(Timestamp deliveryTime) {
    LocalDateTime time = LocalDateTime.ofInstant(
      deliveryTime.toInstant(),
      ZoneId.of("Europe/Madrid")
    );

    if (
      List
        .of(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY)
        .contains(time.getDayOfWeek())
    ) {
      return false;
    }

    int hour = time.getHour();
    return 13 <= hour && hour <= 16;
  }
}
