package app.service;

import app.entity.Product;
import app.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

  @Autowired
  private ProductRepository productRepository;

  public List<Product> getByFilter(
    @RequestParam Optional<Product.Category> category,
    @RequestParam Optional<Boolean> inMenu,
    @RequestParam Optional<Boolean> inDailyMenu
  ) {
    // a null Specification is ignored by Spring Boot
    Specification<Product> withCategory = (product, query, specBuilder) ->
      category
        .map(value -> specBuilder.equal(product.get("category"), value))
        .orElse(null);
    Specification<Product> withIsInMenu = (product, query, specBuilder) ->
      inMenu
        .map(value -> specBuilder.equal(product.get("isInMenu"), value))
        .orElse(null);
    Specification<Product> withIsInDailyMenu = (product, query, specBuilder) ->
      inDailyMenu
        .map(value -> specBuilder.equal(product.get("isInDailyMenu"), value))
        .orElse(null);

    Specification<Product> withFilters = Specification
      .where(withCategory)
      .and(withIsInMenu)
      .and(withIsInDailyMenu);

    return productRepository.findAll(withFilters);
  }

  public List<Product> getAllByIds(List<Long> ids) {
    return this.productRepository.findAllById(ids);
  }

  public Optional<Product> getById(Long id) {
    return productRepository.findById(id);
  }
}
