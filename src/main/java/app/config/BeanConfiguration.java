package app.config;

import app.entity.Menu;
import app.entity.ProductMenu;
import app.model.MenuDTO;
import app.model.ProductDTO;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.List;
import java.util.stream.Collectors;

@Configuration
public class BeanConfiguration {

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
      .select()
      .apis(RequestHandlerSelectors.basePackage("app.controller"))
      .paths(PathSelectors.any())
      .build();
  }

  @Bean
  public ModelMapper modelMapper() {
    ModelMapper modelMapper = new ModelMapper();

    modelMapper
      .typeMap(Menu.class, MenuDTO.class)
      .addMappings(
        mapper -> {
          Converter<List<ProductMenu>, List<ProductDTO>> productConverter = ctx ->
            ctx
              .getSource()
              .stream()
              .map(ProductMenu::getProduct)
              .map(product -> modelMapper.map(product, ProductDTO.class))
              .collect(Collectors.toList());

          mapper
            .using(productConverter)
            .map(Menu::getProductMenuList, MenuDTO::setProducts);
        }
      );

    return modelMapper;
  }
}
