package app.repository;

import app.entity.CustomerOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

import static app.entity.CustomerOrder.State;

public interface CustomerOrderRepository
  extends JpaRepository<CustomerOrder, Long> {
  List<CustomerOrder> findByUserId(Long userId);
  Optional<CustomerOrder> findById(Long id);
  List<CustomerOrder> findByStateIn(List<State> state);
}
