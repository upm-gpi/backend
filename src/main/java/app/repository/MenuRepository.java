package app.repository;

import app.entity.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface MenuRepository extends JpaRepository<Menu, Long> {
  Optional<Menu> findById(Long aLong);

  @Query(
    value = "SELECT m.* " +
    "FROM menu m " +
    "JOIN product_menu pm ON m.id = pm.menu_id " +
    "JOIN product p ON pm.product_id = p.id " +
    "GROUP BY m.id " +
    "HAVING GROUP_CONCAT(p.id ORDER BY p.id) LIKE :listOfIds",
    nativeQuery = true
  )
  Optional<Menu> isAMenu(String listOfIds); // they must be ordered and comma separated. Returns the id of the menu if exists
}
