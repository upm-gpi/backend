package app.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
@Data
public class ProductMenu implements Serializable {

  @Id
  @ManyToOne
  @JoinColumn(name = "product_id")
  private Product product;

  @Id
  @ManyToOne
  @JoinColumn(name = "menu_id")
  private Menu menu;
}
