package app.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Menu {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @OneToMany(mappedBy = "menu", cascade = CascadeType.ALL)
  private List<ProductMenu> productMenuList;

  @OneToMany(mappedBy = "menu", cascade = CascadeType.ALL)
  private List<MenuCustomerOrder> menuCustomerOrderList;
}
