package app.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Product {

  public enum Category {
    entrante,
    ensalada,
    principal,
    postre,
    bebida
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;

  @Enumerated(EnumType.STRING)
  private Category category;

  @Column(name = "is_in_menu")
  private Boolean isInMenu;

  @Column(name = "is_in_daily_menu")
  private Boolean isInDailyMenu;

  private String description;
  private Double price;

  @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
  private List<ProductCustomerOrder> productCustomerOrderList;

  @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
  private List<ProductMenu> productMenuList;
}
