package app.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
@Data
public class ProductCustomerOrder implements Serializable {

  @Id
  @ManyToOne
  @JoinColumn(name = "product_id")
  private Product product;

  @Id
  @ManyToOne
  @JoinColumn(name = "customer_order_id")
  private CustomerOrder customerOrder;

  private Integer amount;
}
