package app.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
@Data
public class MenuCustomerOrder implements Serializable {

  @Id
  @ManyToOne
  @JoinColumn(name = "menu_id")
  private Menu menu;

  @Id
  @ManyToOne
  @JoinColumn(name = "customer_order_id")
  private CustomerOrder customerOrder;

  private Integer amount;
}
