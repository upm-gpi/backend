package app.entity;

import java.sql.Timestamp;
import java.util.List;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
public class CustomerOrder {

  public enum State {
    recibido,
    preparando,
    entregando,
    entregado,
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private Timestamp date;

  @Column(name = "delivery_time")
  private Timestamp deliveryTime;

  @Column(name = "shipping_address")
  private String shippingAddress;

  private Double amount;

  @Enumerated(EnumType.STRING)
  private State state;

  @ManyToOne
  @JoinColumn(name = "user_id")
  private User user;

  @OneToMany(mappedBy = "customerOrder", cascade = CascadeType.ALL)
  private List<ProductCustomerOrder> productCustomerOrderList;

  @OneToMany(mappedBy = "customerOrder", cascade = CascadeType.ALL)
  private List<MenuCustomerOrder> menuCustomerOrderList;
}
