package app.util;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ModelMapperUtils {

  @Autowired
  private ModelMapper modelMapper;

  public <M, D> D map(M model, Class<D> dtoType) {
    D dto = modelMapper.map(model, dtoType);
    return dto;
  }

  public <M, D> List<D> map(List<M> models, Class<D> dtoType) {
    return models
      .stream()
      .map(model -> map(model, dtoType))
      .collect(Collectors.toList());
  }
}
