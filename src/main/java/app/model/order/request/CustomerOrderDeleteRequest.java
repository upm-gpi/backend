package app.model.order.request;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CustomerOrderDeleteRequest {

  @NotNull
  private Long userId;

  @NotNull
  private Long orderId;
}
