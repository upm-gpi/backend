package app.model.order.request;

import lombok.Data;

import static app.entity.CustomerOrder.State;

@Data
public class CustomerOrderUpdateStateRequest {

  private Long userId;
  private Long orderId;
  private State newState;
}
