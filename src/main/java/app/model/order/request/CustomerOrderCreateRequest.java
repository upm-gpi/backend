package app.model.order.request;

import app.validation.order.annotations.ConsistentProductsInMenu;
import app.validation.order.annotations.ExistingProducts;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.sql.Timestamp;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;
import lombok.Data;

@Data
public class CustomerOrderCreateRequest {

  @NotNull
  @FutureOrPresent
  @JsonFormat(shape = JsonFormat.Shape.NUMBER)
  private Timestamp deliveryTime;

  @NotNull
  private String shippingAddress;

  @NotNull
  @Positive
  private Double amount;

  @Positive
  private Long userId;

  @Data
  public static class Product {

    @NotNull
    @Positive
    private Long id;

    @NotNull
    @Positive
    private Integer amount;
  }

  @NotNull
  @Valid
  @ExistingProducts
  private List<Product> products;

  @Data
  public static class Menu {

    @NotNull
    @Positive
    private Integer amount;

    @NotNull
    @Positive
    private Long starterId;

    @NotNull
    @Positive
    private Long mainId;

    @NotNull
    @Positive
    private Long dessertId;

    @NotNull
    @Positive
    private Long drinkId;
  }

  @NotNull
  @Valid
  @ConsistentProductsInMenu
  private List<Menu> menus;
}
