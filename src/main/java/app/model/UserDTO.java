package app.model;

import app.entity.User;
import lombok.Data;

@Data
public class UserDTO {

  private Long id;
  private String name;
  private String address;
  private String email;
  private String password;
  private User.Role role;
}
