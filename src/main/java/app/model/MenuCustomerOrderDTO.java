package app.model;

import lombok.Data;

@Data
public class MenuCustomerOrderDTO {

  private MenuDTO menu;
  private Integer amount;
}
