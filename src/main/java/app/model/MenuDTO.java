package app.model;

import lombok.Data;

import java.util.List;

@Data
public class MenuDTO {
  private Long id;
  private List<ProductDTO> products;
}
