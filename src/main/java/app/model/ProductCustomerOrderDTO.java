package app.model;

import lombok.Data;

@Data
public class ProductCustomerOrderDTO {
  private ProductDTO product;
  private Integer amount;
}
