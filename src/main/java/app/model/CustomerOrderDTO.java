package app.model;

import app.entity.CustomerOrder;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;

@Data
public class CustomerOrderDTO {

  private Long id;

  @JsonFormat(shape = JsonFormat.Shape.NUMBER)
  private Timestamp date;

  @JsonFormat(shape = JsonFormat.Shape.NUMBER)
  private Timestamp deliveryTime;

  private String shippingAddress;
  private Double amount;

  private CustomerOrder.State state;
  private UserDTO user;

  @JsonProperty("products")
  private List<ProductCustomerOrderDTO> productCustomerOrderList;

  @JsonProperty("menus")
  private List<MenuCustomerOrderDTO> menuCustomerOrderList;
}
