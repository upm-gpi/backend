package app.model.product.response;

import app.model.ProductDTO;
import lombok.Data;

import java.util.List;

@Data
public class ProductsDailyMenuResponse {

  List<ProductDTO> starters;
  List<ProductDTO> mains;
  List<ProductDTO> desserts;
  List<ProductDTO> drinks;
}
