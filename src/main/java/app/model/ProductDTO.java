package app.model;

import app.entity.Product;
import lombok.Data;

@Data
public class ProductDTO {

    private Long id;
    private String name;
    private Product.Category category;
    private Boolean isInMenu;
    private Boolean isInDailyMenu;
    private String description;
    private Double price;
}
