package app.model.auth.request;

import app.entity.User;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RegisterRequest {

  private String name;
  private String address;

  @NotNull
  @Email
  private String email;

  @NotNull
  @Size(min = 10, max = 64)
  private String password;

  @NotNull
  private User.Role role;
}
